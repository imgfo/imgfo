# Changelog
This is the event how ImgFo improve their feature for us. If you have any idea for ImgFo, please submit an issues at [here](https://gitlab.com/imgfo/imgfo/-/issues).

## 19 April 2021
- Add validate url on images field
- Fixed Upload Images

---

## 27 March 2021
- Fixed can't login into GitHub Gist
- Change address of backup server

---

## 17 February 2021
- We don't allow user to put their iframe ads anymore.

**Note:**  
- The reason is because there many users put iframe with injected malware.

---

## 12 February 2021
- Now we using [Statically.io](https://statically.io) for automatic SSL and CDNify the images.

**Note:**  
- We no longer use ImgProxify anymore because they suggest us to use their source code for professional or company. Too bad, because of this, we haven't an images proxify feature anymore.
- We will use ImgProxify at the future if we have much balance. Means we need more donatur for this.

---

## 30 January 2021
- Now users are able to put their iframe ads.

---

## 19 January 2021
- Add Upload via WebHook.
- Update documentations and terms of services.

---

## 16 January 2021
- Moved to GitLab.

---

## 13 Januari 2021
- Add Free Image Host Upload.
- Integrated with Gist GitHub.

---

## 06 January 2021
- Update BOT Detection.

---

## 05 November 2020
- Fixed Embed Light Theme.

---

## 04 November 2020
- Add Auto SSL and CDNify with ImgProxify.

---

## 14 September 2020
- Improve performance speed for iframe.

---

## Older
- Many things have been changed, it's very long so we can't list this more at here.

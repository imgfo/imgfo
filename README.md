# ImgFo
ImgFo is a free images reader web application that makes your images content to become easier to read. ImgFo also protecting your content from thieves, so you have no worries to share your original works for public.

Features :
- Auto SSL, CDNify and Proxify
- Your content is strongly protected
- You control the image source
- Lightweight and Fast Loading
- Private but Shareable
- Unlimited Content Files
- No Registration and Free Forever

ImgFo are using **Decentralized Server Concept**, means that all the contents in ImgFo is placed in our users server. So ImgFo don't have any control for it.

Visit [https://imgfo.com](https://imgfo.com)

## Changelog
Any update information will be listed at [here](CHANGELOG.md).

## Advertise with Us
[Want to advertise with us?](mailto:imgfo.dev@gmail.com)  
Your ads will be shown in all content pages of ImgFo website.  
Currently traffic is:  
- 600K++ visitors/monthly  
- 1M++ impressions/monthly

## Our Sponsors
[Want to become our sponsors?](mailto:imgfo.dev@gmail.com)  
Your name, website or company will be put at homepage and repository of ImgFo.
<!-- platinum start -->
<table>
  <tbody>
    <tr>
      <td align="center" valign="middle">
        <a href="https://vercel.com/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-vercel.png">
        </a>
      </td>
      <td align="center" valign="middle">
        <a href="https://cloudflare.com/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-cloudflare.png">
        </a>
      </td>
      <td align="center" valign="middle">
        <a href="https://imgproxify.com/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-imgproxify.png">
        </a>
      </td>
      <td align="center" valign="middle">
        <a href="https://raw.githack.com/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-githack.png">
        </a>
      </td>
    </tr>
    <tr>
      <td align="center" valign="middle">
        <a href="https://gitlab.com/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-gitlab.png">
        </a>
      </td>
      <td align="center" valign="middle">
        <a href="https://github.com/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-github.png">
        </a>
      </td>
      <td align="center" valign="middle">
        <a href="https://fastify.io/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-fastify.png">
        </a>
      </td>
      <td align="center" valign="middle">
        <a href="https://apiembed.com/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-kong.png">
        </a>
      </td>
    </tr>
    <tr>
      <td align="center" valign="middle">
        <a href="https://statically.io/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-statically.png">
        </a>
      </td>
      <td align="center" valign="middle">
        <a href="https://freeimage.host/" target="_blank">
          <img width="200px" src="https://imgfo.com/img/logo-freeimagehost-300.png">
        </a>
      </td>
    </tr>
  </tbody>
</table>
<!-- platinum end -->

## Our Supporters
[Want to become our supporters?](mailto:imgfo.dev@gmail.com)  
Your name, website or company will be listed at here.

**Thank You** very much for this people who already support us:
- Hensuki
- Foomaster
- Xtorrentx
- Oxsilento
- ClipBokep
- What3ve
- [Manganeet](https://manganeet.com)
- 
